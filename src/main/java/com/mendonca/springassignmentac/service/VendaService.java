package com.mendonca.springassignmentac.service;

import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.repository.VendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendaService {

    @Autowired
    private VendaRepository vendaRepository;

    public Venda saveVenda(Venda venda) {
        return vendaRepository.save(venda);
    }

    public List<Venda> getVendas() {
        return vendaRepository.findAll();
    }

    public Venda getVendaById(int id) {
        return vendaRepository.findById(id).orElse(null);
    }

    public String deleteVenda(int id) {
        vendaRepository.deleteById(id);
        return "Venda removed - " + id;
    }

    public Venda updateVenda(Venda venda) {
        Venda existingVenda = vendaRepository.findById(venda.getId()).orElse(venda);
        existingVenda.setVendedor(venda.getVendedor());
        existingVenda.setValor(venda.getValor());
        existingVenda.setCliente(venda.getCliente());
        return vendaRepository.save(existingVenda);
    }

}
