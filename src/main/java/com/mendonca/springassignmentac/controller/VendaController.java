package com.mendonca.springassignmentac.controller;

import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.service.VendaService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@Slf4j
public class VendaController {

    @Autowired
    private VendaService vendaService;

    @PostMapping("/addVenda")
    public Venda addVenda(@RequestBody Venda venda) {
        log.info("addVenda endpoint working.");
        return vendaService.saveVenda(venda);
    }

    @GetMapping("/vendas")
    public List<Venda> findAllVendas() {
        log.info("findAllVendas endpoint working.");
        return vendaService.getVendas();
    }

    @GetMapping("/venda/{id}")
    public Venda findVendaById(@PathVariable int id) {
        log.info("findVendaById endpoint working.");
        return vendaService.getVendaById(id);
    }

    @PutMapping("/update")
    public Venda updateVenda(@RequestBody Venda venda) {
        log.info("updateVenda endpoint working.");
        return vendaService.updateVenda(venda);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteVenda(@PathVariable int id) {
        log.info("deleteVenda endpoint working.");
        return vendaService.deleteVenda(id);
    }

}
