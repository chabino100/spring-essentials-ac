package com.mendonca.springassignmentac.vendaIntegrationTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.repository.VendaRepository;
import org.junit.After;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class VendaControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private VendaRepository vendaRepository;

    @Test
    public void testCreateVenda() throws Exception {
        String nomeCliente = "João";
        Venda newVenda = new Venda(800, nomeCliente, "Fabiola");

        String url = "/addVenda";

        MvcResult mvcResult = mockMvc.perform(post(url).with(user("antoniomendonca").password("password123").roles("ADMIN"))
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(newVenda)))
                .andExpect(status().isOk()).andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        Venda vendaResponse = objectMapper.readValue(response, Venda.class);
        Venda savedVenda = vendaRepository.findById(vendaResponse.getId()).get();

        assertThat(savedVenda.getCliente()).isEqualTo(nomeCliente);
    }

    @Test
    public void testUpdateVenda() throws Exception {
        String nomeCliente = "Paola";
        Venda newVenda = new Venda(1, 800, nomeCliente, "Fabiola");

        String url = "/update";

        MvcResult mvcResult = mockMvc.perform(put(url).with(user("antoniomendonca").password("password123").roles("ADMIN"))
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(newVenda)))
                .andExpect(status().isOk()).andReturn();

        String response = mvcResult.getResponse().getContentAsString();

        Venda vendaResponse = objectMapper.readValue(response, Venda.class);
        Venda savedVenda = vendaRepository.findById(vendaResponse.getId()).get();

        assertThat(savedVenda.getCliente()).isEqualTo(nomeCliente);
    }

    @Test
    public void testListVendas() throws Exception {
        String url = "/vendas";

        MvcResult mvcResult = mockMvc.perform(get(url).with(user("antoniomendonca").password("password123").roles("ADMIN")))
                .andExpect(status().isOk())
                .andReturn();

        String JsonResponse = mvcResult.getResponse().getContentAsString();
        Venda[] vendas = objectMapper.readValue(JsonResponse, Venda[].class);

        assertThat(vendas).hasSize(2);

    }

    @After
    public void testDeleteVenda() throws Exception {
        Integer vendaId = 1;
        String url = "/delete/" + vendaId;

        mockMvc.perform(delete(url).with(user("antoniomendonca").password("password123").roles("ADMIN"))).andExpect(status().isOk());

        assertThat(vendaRepository.findById(vendaId)).isNotPresent();
    }

}
