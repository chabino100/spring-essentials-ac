package com.mendonca.springassignmentac.vendaTest;

import com.mendonca.springassignmentac.controller.VendaController;
import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.repository.VendaRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest(VendaController.class)
public class VendaControllerTest {

    @Autowired
    private VendaController vendaController;

    @MockBean
    private VendaRepository vendaRepository;

    @Test
    public void testCreateVenda() throws Exception {

        Venda newVenda = new Venda(2333.0, "Francisco", "Reinaldo");
        Venda savedVenda = new Venda(1, 2333.0, "Francisco", "Reinaldo");

        Mockito.when(vendaRepository.save(newVenda)).thenReturn(savedVenda);
        assertThat(vendaController.addVenda(newVenda)).isEqualTo(savedVenda);
    }

    @Test
    public void testFindByIdVenda() throws Exception {
        Venda venda = new Venda(1, 878, "Karen", "Marina");

        Integer vendaId = 1;

        Mockito.when(vendaRepository.findById(vendaId)).thenReturn(Optional.of(venda));
        assertThat(vendaController.findVendaById(venda.getId())).isEqualTo(venda);
    }

    @Test
    public void testUpdateVenda() throws Exception {
        Venda existingVenda = new Venda(1, 2333.0, "Rodrigo", "Matheus");
        Venda savedVenda = new Venda(2, 2424.0, "Armando", "Fatima");

        Mockito.when(vendaRepository.save(existingVenda)).thenReturn(savedVenda);
        assertThat(vendaController.updateVenda(existingVenda)).isEqualTo(savedVenda);
    }

    @Test
    public void testDeleteVenda() throws Exception {
        Integer vendaId = 1;

        Mockito.doNothing().when(vendaRepository).deleteById(vendaId);
        vendaController.deleteVenda(vendaId);

        Mockito.verify(vendaRepository).deleteById(vendaId);
    }

    @Test
    public void testListVendas() throws Exception {
        List<Venda> vendaList = new ArrayList<>();
        vendaList.add(new Venda(1, 200, "Rodrigo", "Angela"));
        vendaList.add(new Venda(2, 2333.0, "Francisco", "Reinaldo"));
        vendaList.add(new Venda(3, 42, "Roberta", "Angela"));

        Mockito.when(vendaRepository.findAll()).thenReturn(vendaList);
        assertThat(vendaController.findAllVendas()).isEqualTo(vendaList);
    }


}
