package com.mendonca.springassignmentac.vendaTest;

import com.mendonca.springassignmentac.entity.Venda;
import com.mendonca.springassignmentac.repository.VendaRepository;
import com.mendonca.springassignmentac.service.VendaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ExtendWith(MockitoExtension.class)
public class VendaServiceTest {

    @InjectMocks
    private VendaService vendaService;

    @Mock
    private VendaRepository vendaRepository;

    @Test
    public void testCreateVenda() {
        Venda venda = new Venda();
        venda.setId(1);
        venda.setCliente("Adriana");
        venda.setVendedor("Chosen");
        venda.setValor(500);

        Mockito.when(vendaRepository.save(venda)).thenReturn(venda);

        assertThat(vendaService.saveVenda(venda)).isEqualTo(venda);
    }

    @Test
    public void testGetVendaById() {
        Venda venda = new Venda();
        venda.setId(1);
        venda.setCliente("Ronaldo");
        venda.setVendedor("Gandalf");
        venda.setValor(200);

        Mockito.when(vendaRepository.findById(1)).thenReturn(Optional.of(venda));

        assertThat(vendaService.getVendaById(1)).isEqualTo(venda);
    }

    @Test
    public void testGetAllVendas() {
        Venda venda1 = new Venda();
        venda1.setCliente("Karen");
        venda1.setVendedor("Pedro");
        venda1.setValor(708);

        Venda venda2 = new Venda();
        venda2.setCliente("Karen");
        venda2.setVendedor("Pedro");
        venda2.setValor(708);

        List<Venda> vendaList = new ArrayList<>();
        vendaList.add(venda1);
        vendaList.add(venda2);

        Mockito.when(vendaRepository.findAll()).thenReturn(vendaList);

        assertThat(vendaService.getVendas()).isEqualTo(vendaList);
    }

    @Test
    public void testDeleteVenda() {
        Venda venda = new Venda();
        venda.setId(1);
        venda.setCliente("Maria");
        venda.setVendedor("João");
        venda.setValor(23);

        Mockito.when(vendaRepository.existsById(venda.getId())).thenReturn(false);

        assertFalse(vendaRepository.existsById(venda.getId()));
    }

    @Test
    public void testUpdateVenda() {
        Venda venda = new Venda();
        venda.setId(1);
        venda.setCliente("Katia");
        venda.setVendedor("Jorge");
        venda.setValor(683);

        Mockito.when(vendaRepository.findById(1)).thenReturn(Optional.of(venda));

        venda.setCliente("Reinaldo");
        Mockito.when(vendaRepository.save(venda)).thenReturn(venda);

        assertThat(vendaService.updateVenda(venda)).isEqualTo(venda);
    }

}
